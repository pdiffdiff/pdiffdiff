#!/usr/bin/env python3
# -*-mode: python; coding: utf-8 -*-

import os
import re
import sys
import getopt
import fnmatch
import difflib
import subprocess
import collections

CONSIDER_UNCHANGED = False
ASSUME_BINARY_UNCHANGED = False

class LineType:
    def __init__(self, line, match):
        self.__line = line
        self.__match = match

    def line(self):
        return self.__line

    def match(self):
        return self.__match

    def __lt__(self, other):
        if not isinstance(other, LineType):
            return NotImplemented
        return self.__line < other.__line

    def __le__(self, other):
        if not isinstance(other, LineType):
            return NotImplemented
        return self.__line <= other.__line

    def __ge__(self, other):
        if not isinstance(other, LineType):
            return NotImplemented
        return self.__line >= other.__line

    def __gt__(self, other):
        if not isinstance(other, LineType):
            return NotImplemented
        return self.__line > other.__line

    def __eq__(self, other):
        if not isinstance(other, LineType):
            return NotImplemented
        return self.__line == other.__line

    def __ne__(self, other):
        if not isinstance(other, LineType):
            return NotImplemented
        return self.__line != other.__line

    def __hash__(self):
        return hash(self.__line)

class HeaderLine(LineType):
    RE = re.compile("")

class FileHeader(LineType):
    def raw_filename(self):
        return self.match().group('filename')

    def filename(self, skipnum):
        """Strip SKIPNUM slashes."""
        raw = self.raw_filename()
        if raw == '/dev/null':
            return raw
        else:
            res = raw
            while skipnum > 0:
                res = strip_one(res)
                skipnum = skipnum - 1
            return res

    def filename_score(self):
        if self.SCORE == 0:
            return 0
        if self.raw_filename() == '/dev/null':
            return 0
        else:
            return self.SCORE

class OnlyInFileLine(FileHeader):
    RE = re.compile("^Only in (?P<dir>.*): (?P<file>.*)$")
    SCORE = 5
    def raw_filename(self):
        return self.match().group('dir') + '/' + self.match().group('file')

class BinaryLine(FileHeader):
    RE = re.compile("^Binary files (?P<filename>.*) and .* differ$")
    SCORE = 5

class IndexLine(FileHeader):
    RE = re.compile("^Index: (?P<filename>.*)$")
    SCORE = 5

class EqLine(FileHeader):
    RE = re.compile("^=+$")
    SCORE = 0

class RCSFileLine(FileHeader):
    RE = re.compile("^RCS file: .*/(?P<filename>[^/]+),v$")
    SCORE = 1

class RetrievingLine(FileHeader):
    RE = re.compile("^retrieving revision ")
    SCORE = 0

class DiffCmd(FileHeader):
    RE = re.compile("^diff .* (?P<filename>[^ ]+)$")
    SCORE = 2

class GitIndex(FileHeader):
    RE = re.compile("^index [0-9a-f]+\\.\\.[0-9a-f]+( [0-7]+)?$")
    SCORE = 0

class GitNewFile(FileHeader):
    RE = re.compile("^new file mode [0-7]+$")
    SCORE = 0

class GitDeletedFile(FileHeader):
    RE = re.compile("^deleted file mode [0-7]+$")
    SCORE = 0

class GitModeChange(FileHeader):
    RE = re.compile("^(new|old) mode [0-7]+$")
    SCORE = 0

class FromLine(FileHeader):
    RE = re.compile("^--- (?P<filename>[^\t]+)")
    SCORE = 3

class ToLine(FileHeader):
    RE = re.compile("^\\+\\+\\+ (?P<filename>[^\t]+)")
    SCORE = 4

class FileData(LineType): pass

class HunkStart(FileData):
    RE = re.compile("^@@")

class InHunk(FileData): pass

class ContextLine(InHunk):
    RE = re.compile('^ ')

class ChangedLine(InHunk): pass

class RemovedLine(ChangedLine):
    RE = re.compile('^-')

class AddedLine(ChangedLine):
    RE = re.compile('^\\+')

class NoNewLineAtEndOfFile(InHunk):
    RE = re.compile('^\\\\ No newline at end of file$')

class EOFError(Exception): pass

def strip_one(fn):
    pos = fn.find('/')
    return fn[pos+1:]

def strip_prefix(fn, prefix):
    assert fn.startswith(prefix)
    fn = fn[len(prefix):]
    if prefix[-1] != '/':
        assert fn[0] == '/'
        fn = fn[1:]
    return fn

class LineLexer:
    def __init__(self, fp):
        self.__fp = fp
        self.__line = None
        self.__in_hunk = False
        self.__line_number = 0
        self.__val = self.__next()

    def step(self):
        if self.__val is None:
            raise EOFError("EOF already seen")
        self.__val = self.__next()

    def peek(self):
        return self.__val

    def consume(self):
        rv = self.peek()
        self.step()
        return rv

    RE_TO_TYPE = [
        IndexLine,
        EqLine,
        RCSFileLine,
        RetrievingLine,
        DiffCmd,
        GitIndex,
        GitNewFile,
        GitDeletedFile,
        GitModeChange,
        FromLine,
        ToLine,
        HunkStart,
        OnlyInFileLine,
        BinaryLine,
        HeaderLine,
        ]

    RE_TO_TYPE_HUNK = [
        ContextLine,
        RemovedLine,
        AddedLine,
        NoNewLineAtEndOfFile,
        ]

    def __match_rule(self, rules):
        for klass in rules:
            m = klass.RE.match(self.__line)
            if m is not None:
                return klass(self.__line, m)
        return None

    def __compute_linetype(self):
        if self.__in_hunk:
            rv = self.__match_rule(self.RE_TO_TYPE_HUNK)
            if rv is not None:
                return rv
            self.__in_hunk = False
        rv = self.__match_rule(self.RE_TO_TYPE)
        if isinstance(rv, HunkStart):
            self.__in_hunk = True
        return rv

    def __next(self):
        self.__line = self.__fp.readline()
        self.__line_number += 1
        if self.__line == '':
            return None
        else:
            if len(self.__line) >= 2 and self.__line[-2:] == '\r\n':
                self.__line = self.__line[:-2]
            elif len(self.__line) >= 1 and self.__line[-1:] == '\n':
                self.__line = self.__line[:-1]
            elif len(self.__line) >= 1 and self.__line[-1:] == '\r':
                self.__line = self.__line[:-1]

        return self.__compute_linetype()

    def line_number(self):
        return self.__line_number


class PatchHunk:
    def __init__(self, lexer):
        self.__start = lexer.consume()
        self.__hunk = []
        self.__changes = []
        self.__lines = None
        while isinstance(lexer.peek(), InHunk):
            if isinstance(lexer.peek(), ChangedLine):
                self.__changes.append(lexer.peek())
            self.__hunk.append(lexer.consume())

    def __lt__(self, other):
        if not isinstance(other, PatchHunk):
            return NotImplemented

        if CONSIDER_UNCHANGED:
            return self.__hunk < other.__hunk
        else:
            return self.__changes < other.__changes

    def __le__(self, other):
        if not isinstance(other, PatchHunk):
            return NotImplemented

        if CONSIDER_UNCHANGED:
            return self.__hunk <= other.__hunk
        else:
            return self.__changes <= other.__changes

    def __ge__(self, other):
        if not isinstance(other, PatchHunk):
            return NotImplemented

        if CONSIDER_UNCHANGED:
            return self.__hunk >= other.__hunk
        else:
            return self.__changes >= other.__changes

    def __gt__(self, other):
        if not isinstance(other, PatchHunk):
            return NotImplemented

        if CONSIDER_UNCHANGED:
            return self.__hunk > other.__hunk
        else:
            return self.__changes > other.__changes

    def __eq__(self, other):
        if not isinstance(other, PatchHunk):
            return NotImplemented

        if CONSIDER_UNCHANGED:
            return self.__hunk == other.__hunk
        else:
            return self.__changes == other.__changes

    def __ne__(self, other):
        if not isinstance(other, PatchHunk):
            return NotImplemented

        if CONSIDER_UNCHANGED:
            return self.__hunk != other.__hunk
        else:
            return self.__changes != other.__changes

    def __hash__(self):
        h = 0
        for line in self.__hunk:
            h = h ^ hash(line)
        return h

    def format(self, header):
        return [header + x.line() for x in [self.__start] + self.__hunk]

    def lines(self):
        if self.__lines is None:
            self.__lines = [x.line() for x in self.__hunk]
        return self.__lines

    def changed_lines(self):
        return self.__changes

    def raw_lines(self):
        return self.__hunk

    def significant_lines(self):
        if CONSIDER_UNCHANGED:
            return self.raw_lines()
        else:
            return self.changed_lines()

    def header_line(self):
        return self.__start.line()

bin_id = 0

class BinaryHunk:
    def __init__(self):
        global bin_id

        self.__bin_id = bin_id
        bin_id += 1

    def __lt__(self, other):
        if not isinstance(other, BinaryHunk):
            return NotImplemented

        return self.__bin_id < other.__bin_id

    def __le__(self, other):
        if not isinstance(other, BinaryHunk):
            return NotImplemented

        return self.__bin_id <= other.__bin_id

    def __ge__(self, other):
        if not isinstance(other, BinaryHunk):
            return NotImplemented

        return self.__bin_id >= other.__bin_id

    def __gt__(self, other):
        if not isinstance(other, BinaryHunk):
            return NotImplemented

        return self.__bin_id > other.__bin_id

    def __eq__(self, other):
        if not isinstance(other, BinaryHunk):
            return NotImplemented

        return self.__bin_id == other.__bin_id

    def __ne__(self, other):
        if not isinstance(other, BinaryHunk):
            return NotImplemented

        return self.__bin_id != other.__bin_id

    def __hash__(self):
        return hash(self.__bin_id)

    def format(self, header):
        return []

    def lines(self):
        return []

    def significant_lines(self):
        return []

    def header_line(self):
        return "+- (above binary diff may or may not be the same)"

class PatchFile:
    def __init__(self, lexer, skipnum, renames):
        self.__header = []
        self.__hunks = []
        self.__fn = None
        self.__score = 0
        while isinstance(lexer.peek(), FileHeader):
            line = lexer.peek()
            lexer.step()
            self.__header.append(line)
            if line.filename_score() > self.__score:
                self.__score = line.filename_score()
                self.__fn = line.filename(skipnum)
                if renames is not None and self.__fn in renames:
                    print(f"Match: {self.__fn} => {renames[self.__fn]}")
                    self.__fn = renames[self.__fn]
                else:
                    print(f"No match for {self.__fn} in {renames}")
            if isinstance(line, OnlyInFileLine):
                return
            if isinstance(line, BinaryLine):
                if not ASSUME_BINARY_UNCHANGED:
                    self.__hunks.append(BinaryHunk())
                return
        while isinstance(lexer.peek(), HunkStart):
            self.__hunks.append(PatchHunk(lexer))

    def filename(self):
        return self.__fn

    def summary(self):
        print("        Header: %d lines" % len(self.__header))
        print("        Hunks: %d" % len(self.__hunks))

    def diff(self, newer, delta):
        header_added = False
        s = difflib.SequenceMatcher(None, self.__hunks, newer.__hunks)
        for tag, i1, i2, j1, j2 in s.get_opcodes():
            if tag != 'equal' and not header_added:
                delta.add_header('  ', self.__header)
                header_added = True
            if tag == 'replace':
                delta.replace(self.__hunks[i1:i2], newer.__hunks[j1:j2])
            elif tag == 'delete':
                if i1 != i2:
                    delta.remove(self.__hunks[i1:i2])
            elif tag == 'insert':
                if j1 != j2:
                    delta.add(newer.__hunks[j1:j2])
            elif tag != 'equal':
                raise ValueError("SequenceMatcher returned a bogus tag", tag)

    def diff_added(self, delta):
        delta.add_header('+ ', self.__header)
        delta.add(self.__hunks)

    def diff_removed(self, delta):
        delta.add_header('- ', self.__header)
        delta.remove(self.__hunks)


class PatchParser:
    def __init__(self, fn, skipnum, handle_git, renames=None):

        if handle_git and fn.startswith("git:"):
            self.__filename = fn[4:]
            proc = subprocess.Popen(["git", "show", fn[4:]],
                                    text=True,
                                    stdout=subprocess.PIPE)
            fp = proc.stdout
        else:
            self.__filename = fn
            proc = None
            fp = open(fn)
        lexer = LineLexer(fp)

        self.__header = []
        while isinstance(lexer.peek(), HeaderLine):
            self.__header.append(lexer.consume())

        self.__files = {}
        while isinstance(lexer.peek(), FileHeader):
            file = PatchFile(lexer, skipnum, renames)
            self.__files.setdefault(file.filename(), []).append(file)

        self.__footer = []
        while isinstance(lexer.peek(), HeaderLine):
            self.__footer.append(lexer.consume())

        if lexer.peek() is not None:
            sys.stderr.write(
                "%s:%d: warning: malformed patch, treated as footer.\n" % (
                fn, lexer.line_number()))
            while lexer.peek() is not None:
                self.__footer.append(lexer.consume())

        fp.close()

    def filename(self):
        return self.__filename

    def summary(self):
        print("  Filename:", self.filename())
        print("    Header: %d lines" % len(self.__header))
        print("    Footer: %d lines" % len(self.__footer))
        print("    Patched files: %d" % len(self.__files))
        for fn, files in list(self.__files.items()):
            print("      %s" % fn)
            for file in files:
                file.summary()

    def diff(self, newer):
        delta = PatchDiff(self.filename(), newer.filename())

        if self.__header != newer.__header:
            delta.header_diff(self.__header, newer.__header)
        if self.__footer != newer.__footer:
            delta.footer_diff(self.__footer, newer.__footer)

        old_files = set(self.__files.keys())
        new_files = set(newer.__files.keys())

        all_files = list(old_files | new_files)
        all_files.sort()

        for patched_file in all_files:

            old = self.__files.get(patched_file, [])
            new = newer.__files.get(patched_file, [])

            common = min(len(old), len(new))

            for ix in range(common):
                old[ix].diff(new[ix], delta)

            for ix in range(common, len(old)):
                old[ix].diff_removed(delta)

            for ix in range(common, len(new)):
                new[ix].diff_added(delta)

        delta.report()

class PatchDiff:
    def __init__(self, afn, bfn):
        self.__afn = afn
        self.__bfn = bfn
        self.__header_diff = []
        self.__footer_diff = []
        self.__hunk_diffs = []

    def __diff_lines(self, a, b):
        return list(difflib.unified_diff([x.line() for x in a],
                                         [x.line() for x in b]))[2:]

    def header_diff(self, aheader, bheader):
        self.__header_diff = self.__diff_lines(aheader, bheader)

    def footer_diff(self, afooter, bfooter):
        self.__footer_diff = self.__diff_lines(afooter, bfooter)

    def add_header(self, fmt, headers):
        for header in headers:
            self.__hunk_diffs.append(fmt + header.line())

    def replace(self, ahunks, bhunks):
        if len(ahunks) > len(bhunks):
            self.__replace(ahunks, bhunks, '+ ', '- ')
        else:
            self.__replace(bhunks, ahunks, '- ', '+ ')

    def __replace(self, longer, shorter, added_fmt, removed_fmt):
        assert len(longer) > 0
        assert len(shorter) > 0

        s = difflib.SequenceMatcher()

        self.__scores = collections.defaultdict(dict)

        for short_ix in range(len(shorter)):
            s.set_seq2(shorter[short_ix].significant_lines())

            for long_ix in range(len(longer)):
                s.set_seq1(longer[long_ix].significant_lines())
                self.__scores[long_ix][short_ix] = s.ratio()

        self.__ctr = [None] * len(shorter)
        self.__best_score = 0
        self.__best = self.__ctr[:]
        self.__find_best(0, longer, shorter)

        long_ix = 0
        for short_ix in range(len(shorter)):
            if self.__best[short_ix] is None:
                self.__format_hunks(added_fmt, [shorter[short_ix]])
            else:
                while long_ix < self.__best[short_ix]:
                    self.__format_hunks(removed_fmt, [longer[long_ix]])
                    long_ix += 1
                self.__format_edited_hunk(longer[long_ix],
                                          shorter[short_ix],
                                          added_fmt,
                                          removed_fmt)
                long_ix += 1
        while long_ix < len(longer):
            self.__format_hunks(removed_fmt, [longer[long_ix]])
            long_ix += 1

    def __find_best(self, short_ix, longer, shorter):
        score = self.__compute_score()
        if score > self.__best_score:
            self.__best_score = score
            self.__best = self.__ctr[:]

        if short_ix >= len(shorter):
            return
        if short_ix == 0:
            minval = 0
        else:
            for ix in range(short_ix - 1, -1, -1):
                if self.__ctr[ix] != None:
                    minval = self.__ctr[ix] + 1
                    break
            else:
                minval = 0

        greedy_best = 0
        self.__find_best(short_ix + 1, longer, shorter)
        for long_ix in range(minval, len(longer)):
            self.__ctr[short_ix] = long_ix
            local_score = self.__scores[long_ix][short_ix]
            if local_score <= greedy_best:
                continue
            greedy_best = local_score
            self.__find_best(short_ix + 1, longer, shorter)
        self.__ctr[short_ix] = None

    def __compute_score(self):
        score = 0
        for short_ix in range(len(self.__ctr)):
            long_ix = self.__ctr[short_ix]
            if long_ix is not None:
                score += self.__scores[long_ix][short_ix]
        return score

    def __format_edited_hunk(self, a, b, added_fmt, removed_fmt):
        s = difflib.SequenceMatcher(None, a.lines(), b.lines())
        r = self.__hunk_diffs
        if a.header_line() == b.header_line():
            r.append('  ' + a.header_line())
        else:
            r.append(added_fmt + b.header_line())
            r.append(removed_fmt + a.header_line())
        for tag, i1, i2, j1, j2 in s.get_opcodes():
            if tag == 'replace':
                for i in range(i1, i2):
                    r.append(removed_fmt + a.lines()[i])
                for i in range(j1, j2):
                    r.append(added_fmt + b.lines()[i])
            elif tag == 'delete':
                for i in range(i1, i2):
                    r.append(removed_fmt + a.lines()[i])
            elif tag == 'insert':
                for i in range(j1, j2):
                    r.append(added_fmt + b.lines()[i])
            elif tag == 'equal':
                if j2 - j1 < 7:
                    for i in range(j1, j2):
                        r.append('  ' + b.lines()[i])
                else:
                    for i in range(j1, j1 + 3):
                        r.append('  ' + b.lines()[i])
                    r.append("[...]")
                    for i in range(j2 - 3, j2):
                        r.append('  ' + b.lines()[i])
            else:
                raise ValueError("SequenceMatcher returned a bogus tag", tag)

    def remove(self, hunks):
        self.__format_hunks('- ', hunks)

    def add(self, hunks):
        self.__format_hunks('+ ', hunks)

    def __format_hunks(self, fmt, hunks):
        for hunk in hunks:
            self.__hunk_diffs.extend(hunk.format(fmt))

    def __optimize_hunk_diffs(self):
        state = 0
        for ix in range(len(self.__hunk_diffs)):
            leader = self.__hunk_diffs[ix][0:2]
            if state == 0:
                if leader == '+ ':
                    first_plus = ix
                    state = 1
            elif state == 1:
                if leader == '- ':
                    first_minus = ix
                    state = 2
                elif leader != '+ ':
                    state = 0
            elif state == 2:
                if leader != '- ':
                    minus_rows = self.__hunk_diffs[first_minus:ix]
                    self.__hunk_diffs[first_minus:ix] = []
                    self.__hunk_diffs[first_plus:first_plus] = minus_rows
                    state = 0


    def report(self):
        self.__optimize_hunk_diffs()
        if (len(self.__header_diff)
            + len(self.__footer_diff)
            + len(self.__hunk_diffs)) > 0:

            print("---", self.__afn)
            print("+++", self.__bfn)

            print('\n'.join(self.__header_diff))
            print('\n'.join(self.__hunk_diffs))
            print('\n'.join(self.__footer_diff))


class Skipper:
    def __init__(self):
        self.__patterns = []

    def add_pattern(self, pattern):
        self.__patterns.append(pattern)

    def excluded(self, filename):
        for pattern in self.__patterns:
            if fnmatch.fnmatch(filename, pattern):
                return True
        return False


class PatchDir:
    def __init__(self, dirname, skipper, skipnum, renames=None):
        self.__patchfiles = {}
        for root, dirs, files in os.walk(dirname):
            for file in files:
                if skipper.excluded(file):
                    continue
                fn = os.path.join(root, file)
                patch = PatchParser(fn, skipnum, False, renames)
                self.__patchfiles[strip_prefix(fn, dirname)] = patch
            for dir in dirs[:]:
                if skipper.excluded(dir):
                    dirs.remove(dir)

    def summary(self):
        print("%d patch files" % len(self.__patchfiles))
        for fn, patch in self.__patchfiles.items():
            print("  File: %s" % fn)
            patch.summary()

    def diff(self, newer):
        old_fn = set(self.__patchfiles.keys())
        new_fn = set(newer.__patchfiles.keys())
        for fn in old_fn - new_fn:
            print("------ DEL FILE:", fn)
        for fn in new_fn - old_fn:
            print("++++++ ADD FILE:", fn)
        for fn in old_fn & new_fn:
            self.__patchfiles[fn].diff(newer.__patchfiles[fn])

def usage(exval):
    if exval == os.EX_OK:
        fp = sys.stdout
    else:
        fp = sys.stderr
    fp.write("Usage: %s [ opts ] older newer\n" % os.path.basename(sys.argv[0]))
    fp.write("\nRecognized options:\n\n")
    fp.write(" -x --skip  glob   Ignore matching file when finding patches.\n")
    fp.write(" -i --ignore-std   Add standard ignore patterns.\n")
    fp.write(" -r --recursive    older and newer are maybe directories.\n")
    fp.write(" -u --unchanged    Ponder unedited lines when comparing hunks.\n")
    fp.write(" -s --summary      Just print a summary of the patch files.\n")
    fp.write(" -b --binary       Ignore binary file diffs.\n")
    fp.write(" -h --help         Show this help.\n")
    fp.write(" -p old,new        Number of slashes to skip on older and newer patch\n")
    fp.write(" --rename=old:new  Handle renamed files.\n")
    fp.write("\n")
    fp.write("If older or newer starts with \"git:\" the rest is a git committish.\n")
    fp.write("\n")
    fp.write("Multiple --rename options may be supplied. They are applied to\n")
    fp.write("patches in OLDER before comparing to the patches in NEWER.\n")
    fp.flush()
    sys.exit(exval)

def main(argv):
    global CONSIDER_UNCHANGED
    global ASSUME_BINARY_UNCHANGED

    skip = Skipper()
    recursive = False
    summary = False
    old_skip = 1
    new_skip = 1
    renames = {}

    opts, args = getopt.getopt(argv[1:], 'x:irhusp:b', [
            'skip=', "ignore-std", "recursive", "unchanged", "binary",
            'rename=',
            "summary", "help" ])

    for opt, arg in opts:
        if opt in ('-x', '--skip'):
            skip.add_pattern(arg)
        elif opt in ('-u', '--unchanged'):
            CONSIDER_UNCHANGED = True
        elif opt in ('-r', '--recursive'):
            recursive = True
        elif opt in ('-s', '--summary'):
            summary = True
        elif opt in ('-i', '--ignore-std'):
            skip.add_pattern('.hg')
            skip.add_pattern('*~')
            skip.add_pattern('#*#')
            skip.add_pattern('*.pyo')
            skip.add_pattern('*.pyc')
            skip.add_pattern('RCS')
            skip.add_pattern('CVS')
            skip.add_pattern('.svn')
        elif opt in ('-b', '--binary'):
            ASSUME_BINARY_UNCHANGED = True
        elif opt in ('-h', '--help'):
            usage(os.EX_OK)
        elif opt in ('-p', ):
            lst = arg.split(",")
            if len(lst) != 2:
                usage(os.EX_USAGE)
            old_skip = int(lst[0])
            new_skip = int(lst[1])
        elif opt in ('--rename', ):
            lst = arg.split(':')
            if len(lst) != 2:
                usage(os.EX_USAGE)
            renames[lst[0]] = lst[1]

    if len(args) != 2:
        usage(os.EX_USAGE)

    if recursive and os.path.isdir(args[0]) and os.path.isdir(args[1]):
        a = PatchDir(args[0], skip, old_skip, renames)
        b = PatchDir(args[1], skip, new_skip)
    else:
        a = PatchParser(args[0], old_skip, True, renames)
        b = PatchParser(args[1], new_skip, True)

    if summary:
        a.summary()
        b.summary()
        return

    a.diff(b)


if __name__ == '__main__':
    main(sys.argv)
