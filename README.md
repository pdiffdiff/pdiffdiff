## pdiffdiff

`pdiffdiff` is a utility to compare two diff files (also called patch
files).  It parses the diff files, compares the deltas, and prints a
diff of differences.

You can use `diff` to compare diff files, but `pdiffdiff` is more
useful.  Because `pdiffdiff` actually understands what changes the
diff files represents, it will ignore superficial changes that still
represent the same differences.  For example, it considers these
patches to be identical, even though they list the files in different
orders, and are generated using `diff -u` and `diff -u2`:

```diff
diff -u -r hello-1/README hello-2/README
--- hello-1/README	2023-02-12 21:33:13.233485397 +0100
+++ hello-2/README	2023-02-12 21:32:54.989300899 +0100
@@ -1 +1 @@
-The standard greeting program.
+Hello, world!
diff -u -r hello-1/hello.c hello-2/hello.c
--- hello-1/hello.c	2023-02-12 21:32:13.968887390 +0100
+++ hello-2/hello.c	2023-02-12 21:32:32.185070789 +0100
@@ -2,5 +2,5 @@
 
 int main()
 {
-  printf("hello, world\n");
+  puts("hello, world");
 }
```

```diff
diff -u2 -r hello-1/hello.c hello-2/hello.c
--- hello-1/hello.c	2023-02-12 21:32:13.968887390 +0100
+++ hello-2/hello.c	2023-02-12 21:32:32.185070789 +0100
@@ -3,4 +3,4 @@
 int main()
 {
-  printf("hello, world\n");
+  puts("hello, world");
 }
diff -u2 -r hello-1/README hello-2/README
--- hello-1/README	2023-02-12 21:33:13.233485397 +0100
+++ hello-2/README	2023-02-12 21:32:54.989300899 +0100
@@ -1 +1 @@
-The standard greeting program.
+Hello, world!
```

But if we compare the first of the above patches with this patch:

```diff
diff -u -r hello-1/README hello-3/README
--- hello-1/README	2023-02-12 21:33:13.233485397 +0100
+++ hello-3/README	2023-02-12 21:45:20.186790632 +0100
@@ -1 +1 @@
-The standard greeting program.
+Hello, world!
diff -u -r hello-1/hello.c hello-3/hello.c
--- hello-1/hello.c	2023-02-12 21:32:13.968887390 +0100
+++ hello-3/hello.c	2023-02-12 21:42:51.481635676 +0100
@@ -2,5 +2,5 @@
 
 int main()
 {
-  printf("hello, world\n");
+  printf("Hello, world!\n");
 }
```

Then it will produce this diff of diffs:

```diff
--- hello1.patch
+++ hello3.patch

  diff -u -r hello-1/hello.c hello-2/hello.c
  --- hello-1/hello.c	2023-02-12 21:32:13.968887390 +0100
  +++ hello-2/hello.c	2023-02-12 21:32:32.185070789 +0100
  @@ -2,5 +2,5 @@
   
   int main()
   {
  -  printf("hello, world\n");
- +  puts("hello, world");
+ +  printf("Hello, world!\n");
   }

```

Here, we can see that both patches remove the `printf` line, but we
also see that the old patch adds a `puts` line, which the new patch
adds a different `printf` line.  Since both patches make the exact
same change to the `README` file, it isn't mentioned in the output.
