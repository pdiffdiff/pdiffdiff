all:

install:
	cp pdiffdiff.py /usr/local/bin/pdiffdiff

check:
	$(MAKE) -C tests check
